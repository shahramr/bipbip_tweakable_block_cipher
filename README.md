# BipBip: A Low-Latency Tweakable Block Cipher with Small Dimensions

This repository contains the reference c++ code for BipBip tweakable block cipher proposed in the paper "BipBip: A Low-Latency Tweakable Block Cipher with Small Dimensions", TCHES Volume 2023, Issue 1.
https://tches.iacr.org/index.php/TCHES/article/view/9955
